import React from 'react';
import { render } from 'react-dom'
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App'
/*import store from './app/store';*/
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import todoApp from './reducers/reducers.js'

const store = createStore(todoApp)

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
